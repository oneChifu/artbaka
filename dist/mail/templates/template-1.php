<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="format-detection" content="telephone=no">
	<title>Письмо от Artbaka.com.ua</title>

	
</head>

<body style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-family: Arial, Helvetica, sans-serif; margin: 0; padding: 0;" bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#ffffff" style="border-spacing: 0; margin: auto; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
	<tr>
		<td class="main" align="center" valign="top" bgcolor="#ffffff" style="background-color: #ffffff; border: 3px solid #d7d7d7; border-collapse: collapse;">
			<div class="head" style="background: #0069b4; color: #ffffff; padding: 20px 15px;">
				<div class="title" style="color: #ffffff; font-size: 24px; font-weight: 700; line-height: 30px; margin-bottom: 10px;">Вам письмо с сайта<br><a href="<?php echo $data['main_url']; ?>" target="_blank" style="color: #ffffff; text-decoration: none;"><?php echo $data['main_url_title']; ?></a></div>
				<div class="subtitle" style="color: #ffffff; font-size: 16px; font-weight: 400; line-height: 20px;">Возможно это заказ, не игнорируйте ;)</div>
			</div>

			<table class="content" style="border-spacing: 0; margin: 20px 0; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
				<tr>
					<td class="content__title" style="border-collapse: collapse; color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: 700; line-height: 18px; padding: 6px 15px; padding-top: 8px; text-align: right; vertical-align: top; width: 150px;">От:</td>
					<td class="content__value" style="border-collapse: collapse; color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding: 6px 15px; text-align: left; vertical-align: top;"><?php echo $data['user_name']; ?></td>
				</tr>
				<tr>
					<td class="content__title" style="border-collapse: collapse; color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: 700; line-height: 18px; padding: 6px 15px; padding-top: 8px; text-align: right; vertical-align: top; width: 150px;">Его номер телефона:</td>
					<td class="content__value" style="border-collapse: collapse; color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding: 6px 15px; text-align: left; vertical-align: top;"><a href="callto:<?php echo $data['user_phone']; ?>" style="color: #0069b4; text-decoration: none;"><?php echo $data['user_phone']; ?></a></td>
				</tr>
				<tr>
					<td class="content__title" style="border-collapse: collapse; color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: 700; line-height: 18px; padding: 6px 15px; padding-top: 8px; text-align: right; vertical-align: top; width: 150px;">Его IP:</td>
					<td class="content__value" style="border-collapse: collapse; color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding: 6px 15px; text-align: left; vertical-align: top;"><?php echo $data['user_ip']; ?></td>
				</tr>
				<?php if ( $data['user_message'] != '' ) : ?>
					<tr>
						<td class="content__title" style="border-collapse: collapse; color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: 700; line-height: 18px; padding: 6px 15px; padding-top: 8px; text-align: right; vertical-align: top; width: 150px;">Также он пишет:</td>
						<td class="content__value" style="border-collapse: collapse; color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding: 6px 15px; text-align: left; vertical-align: top;"><?php echo $data['user_message']; ?></td>
					</tr>
				<?php endif; ?>
				<?php if ( $data['user_file'] != '' ) : ?>
					<tr>
						<td class="content__center" colspan="2" style="border-collapse: collapse; font-family: Arial, Helvetica, sans-serif; font-weight: 700; padding: 30px 15px 10px 15px; text-align: center; vertical-align: top;">Ещё он прислал картинку</td>
					</tr>
				<?php endif; ?>
			</table>

			<div class="social" style="margin: 30px 0 10px 0;">
                <a href="http://vk.com/clubfairyart" target="_blank" class="social__item" title="Vkontakte" style="display: inline-block; height: 42px; margin: 0 10px; width: 42px;"><img src="<?php echo $data['main_url']; ?>/img/icons/icon-social-vk-hover.png" alt="Vkontakte" style="-ms-interpolation-mode: bicubic; display: block; width: 100%;"></a>
                <a href="https://www.instagram.com/graffiti_artbaka/" target="_blank" class="social__item" title="Instagram" style="display: inline-block; height: 42px; margin: 0 10px; width: 42px;"><img src="<?php echo $data['main_url']; ?>/img/icons/icon-social-ig-hover.png" alt="Instagram" style="-ms-interpolation-mode: bicubic; display: block; width: 100%;"></a>
                <a href="https://www.facebook.com/people/%D0%90%D0%BB%D0%B5%D0%BA%D1%81%D0%B5%D0%B9-%D0%91%D0%B0%D0%BA%D0%B0/100005350004824" target="_blank" class="social__item" title="Facebook" style="display: inline-block; height: 42px; margin: 0 10px; width: 42px;"><img src="<?php echo $data['main_url']; ?>/img/icons/icon-social-fb-hover.png" alt="Facebook" style="-ms-interpolation-mode: bicubic; display: block; width: 100%;"></a>
            </div>

            <div class="footer" style="background: #fa2a2a; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 16px; font-weight: 700; line-height: 24px; padding: 30px 15px;">
            	Наши телефоны: <br><a href="callto:+380962796005" style="color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 16px; font-weight: 700; line-height: 24px; text-decoration: underline;">096 279-60-05</a><br><a href="callto:+380996762122" style="color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 16px; font-weight: 700; line-height: 24px; text-decoration: underline;">099 676-21-22</a>
            </div>
		</td>
	</tr>
</table>
</body>
</html>