<?php
if( !isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
    die('Запрос должен быть Ajax POST');
}

if ( !isset($_POST['action']) && $_POST['action'] != 'mail' ) {
	die('Здесь рыбы нет!');
}


function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

function sendMail($data) {
	require('PHPMailer/PHPMailerAutoload.php');
	$mail = new PHPMailer;

	// $mail->setLanguage('ru', 'PHPMailer/language/');

	// $mail->SMTPDebug = 3;                               // Enable verbose debug output
	// $mail->CharSet = 'UTF-8';
	// $mail->isSMTP();                                      // Set mailer to use SMTP
 //    $mail->Host = 'smtp.gmail.com;smtp-relay.gmail.com'; // Specify main and backup SMTP servers
 //    $mail->Port = 465;                                    // TCP port to connect to
 //    $mail->Mailer = 'smtp';
	// $mail->SMTPAuth = true;                               // Enable SMTP authentication
	// $mail->Username = 'bytadat@gmail.com';                 // SMTP username
	// $mail->Password = 'qnx4meoxy';                           // SMTP password
	// $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
	
	$mail->setFrom('artbakac@artbaka.com.ua', 'Artbaka.com.ua');
	$mail->addAddress($data['admin_email']);     // Add a recipient

	$mail->addAttachment($data['user_file']['tmp_name'], $data['user_file']['name']);         // Add attachments
	$mail->isHTML(true);                                  // Set email format to HTML

	$mail->Subject = 'Заявка с сайта artbaka.com.ua';
	// $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

	ob_start();
	require('templates/template-1.php');
	$mail->Body = ob_get_contents();
	ob_get_clean();
	ob_end_flush();

	return $mail->send();
}


// Валидація гугл капчі
function validateCaptcha() {
	$url = 'https://www.google.com/recaptcha/api/siteverify';
	$data = array(
		'secret' => '6LfqtiYUAAAAABDaOJpS4pWJVpjTE-2TXVfVlpyN',
		'response' => $_POST["g-recaptcha-response"]
	);
	$options = array(
		'http' => array (
			'method' => 'POST',
			'content' => http_build_query($data)
		)
	);
	$context  = stream_context_create($options);
	$verify = file_get_contents($url, false, $context);
	$captcha_success = json_decode($verify);

	return $captcha_success->success;
}


$data = array();
$data['admin_email'] = 'onechifu@gmail.com';
$data['main_url'] = 'http://artbaka.com.ua/new2';
$url_parts = parse_url($data['main_url']);
$data['main_url_title'] = $url_parts['host'];

$data['user_name'] = filter_var($_POST["user_name"], FILTER_SANITIZE_STRING);
$data['user_phone'] = preg_replace("/[^0-9]/","", filter_var($_POST["user_phone"], FILTER_SANITIZE_NUMBER_INT));
$data['user_message'] = filter_var($_POST["user_message"], FILTER_SANITIZE_STRING);
$data['user_ip'] = get_client_ip();
$data['user_file'] = $_FILES['user_file'];



if( strlen($data['user_name']) < 4 ){ // If length is less than 4 it will output JSON error.
    echo json_encode(array('ok' => false, 'validate' => true, 'text' => 'Имя слишком короткое'));
    exit;
}
if( strlen($data['user_phone']) < 10 ) { // If length is less than 4 it will output JSON error.
    echo json_encode(array('ok' => false, 'validate' => true, 'text' => 'Введите номер телефона'));
    exit;
}
$data['user_phone'] = preg_replace("/([0-9]{3})([0-9]{3})([0-9]{2})([0-9]{2})/", "($1) $2-$3-$4", $data['user_phone']);

if( count($data['user_file']['name']) > 0 ) {
	$allowed_file_type = array('jpeg','jpg', 'png');
	$filename = $data['user_file']['name'];
	$ext = pathinfo($filename, PATHINFO_EXTENSION);

	if(!in_array($ext, $allowed_file_type) ) {
	    echo json_encode(array('ok' => false, 'validate' => true, 'text' => 'Доступные форматы файла JPG, JPEG, PNG'));
	    exit;
	}

	if( $data['user_file']['size'] > 1024*1000*3 ) {
	    echo json_encode(array('ok' => false, 'validate' => true, 'text' => 'Файл должен быть меньше чем 3 mb'));
	    exit;
	}
}


$result = array(
	'ok' => false
);
if ( validateCaptcha() ) {
	if ( sendMail($data) ) {
		$result['ok'] = true;
	}
} else {
	$result['recaptcha'] = false;
}

echo json_encode($result);
?>