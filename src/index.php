<!doctype html>
<html lang="ru">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="img/logo.png" />
    <link rel="icon" type="image/png" href="img/favicon.png" />
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" sizes="57x57" href="apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
	<link rel="manifest" href="manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">


    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <title>Artbaka</title>
    <meta name="keywords" content="design, art, graffiti">
    <meta name="description" content="Художественная роспись интерьера и экстерьеров от лючших мастеров из Artbaka" />

    <meta itemprop="name" content="Artbaka">
    <meta itemprop="description" content="Художественная роспись интерьера и экстерьеров от лючших мастеров из Artbaka">
    <meta itemprop="image" content="img/logo.png">

    <meta property="fb:app_id" content="322756301476819">
    <meta property="og:title" content="Artbaka" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="http://artbaka.com.ua" />
    <meta property="og:image" content="img/logo.png" />
    <meta property="og:description" content="Художественная роспись интерьера и экстерьеров от лючших мастеров из Artbaka" />
    <meta property="og:site_name" content="Художественная роспись интерьера и экстерьеров от лючших мастеров из Artbaka" />
	
	<!-- build:csslibs -->
    <link href="css/libs.css" rel="stylesheet" />
    <!-- endbuild -->
    <!-- build:cssmain -->
    <link href="css/main.css" rel="stylesheet" />
    <!-- endbuild -->
</head>

<body>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-100862558-2', 'auto');
ga('send', 'pageview');
</script>

<div id="wrapper" class="wrapper" data-role="page">
    <header class="head">
        <div class="container">
            <div class="head__phones">
                <div class="head__phones_inner">
                    <div class="head__phones__title">Тел.:</div>
                    <div class="head__phones__content">
                        <a href="callto:+380962796005" class="head__phones__phone">096 279-60-05</a>
                        <a href="callto:+380996762122" class="head__phones__phone">099 676-21-22</a>
                    </div>
                    <i class="icon icon-phone"></i>
                </div>
            </div>
            
            <div class="head__blocks">
                <div class="head__block head__block-exterior">
                    <div class="head__block_inner">
                        <div class="head__block__info">
                            <div class="head__block__title"><span>экстерьер</span></div>
                            <div class="head__block__content">
                                <span>Мы можем изменить Ваш фасад так, что он будет не только украшать, но и продавать.</span>
                                <div class="head__block__btn btn btn-blue js-changetype_btn" data-section="exterior">детали</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="head__block head__block-interior">
                    <div class="head__block_inner">
                        <div class="head__block__info">
                            <div class="head__block__title"><span>интерьер</span></div>
                            <div class="head__block__content">
                                <span>Рисунок в интерьере будет заполнять пустоту и радовать Ваших гостей.</span>
                                <div class="head__block__btn btn btn-blue js-changetype_btn" data-section="interior">детали</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="social social-head">
                <div class="social_inner">
                    <a href="http://vk.com/clubfairyart" class="social__item" target="_blank"><i class="icon icon-social-vk"></i></a>
                    <a href="https://www.instagram.com/graffiti_artbaka/" class="social__item" target="_blank"><i class="icon icon-social-ig"></i></a>
                    <a href="https://www.facebook.com/people/%D0%90%D0%BB%D0%B5%D0%BA%D1%81%D0%B5%D0%B9-%D0%91%D0%B0%D0%BA%D0%B0/100005350004824" class="social__item" target="_blank"><i class="icon icon-social-fb"></i></a>
                </div>
            </div>
        </div>
    </header>


    <section id="steps">
        <div class="container">
            <h2>пять шагов к искусству</h2>

            <div class="section__subtitle">для начала работы над Вашим эскизом</div>
            
            <div class="steps__icons">
                <div class="steps__icons_inner">
                    <div class="steps__item-icon" data-step="1">
                        <i class="icon icon-step-1"></i>
                    </div>
                    
                    <div class="steps__item-icon" data-step="2">
                        <i class="icon icon-step-2"></i>
                    </div>
                    
                    <div class="steps__item-icon" data-step="3">
                        <i class="icon icon-step-3"></i>
                    </div>
                    
                    <div class="steps__item-icon" data-step="4">
                        <i class="icon icon-step-4"></i>
                    </div>

                    <div class="steps__item-icon" data-step="5">
                        <i class="icon icon-step-5"></i>
                    </div>
                </div>
            </div>

            <div class="steps__descs">
                <div class="steps__item-desc" data-step="1"><div class="steps__item-desc_inner">1. Вы можете вызвать мастера на объект, позвонив ему, или заполнив форму ниже</div></div>
                <div class="steps__item-desc" data-step="2"><div class="steps__item-desc_inner">2. Мастер проконсультирует Вас по поводу материалов и бюджета, обсудит с Вами идеи для проекта.</div></div>
                <div class="steps__item-desc" data-step="3"><div class="steps__item-desc_inner">3. Вы получите авторский эскиз и он будет учитывать все Ваши пожелания</div></div>
                <div class="steps__item-desc" data-step="4"><div class="steps__item-desc_inner">4. Мы приступаем к реализации рисунка, используя влагостойкую спрей-краску</div></div>
                <div class="steps__item-desc" data-step="5">
                	<div class="steps__item-desc_inner exterior">5. Вы получаете рисунок, который привлекает внимание и долго держится на стене</div>
                	<div class="steps__item-desc_inner interior">5. Вы получаете рисунок, который заполняет пустоту и радует Ваших гостей</div>
            	</div>
            </div>

            <div class="calltoform">
                <button class="btn btn-default-line calltoform__btn"><span>отправить фото</span></button>
                <div class="calltoform__desc">После заполнения формы Вам перезвонит мастер и уточнит детали</div>
            </div>
        </div>
    </section>
    

    <section id="examples">
        <div class="container">
            <h2>цены на примерах</h2>

            <div class="section__subtitle">ознакомьтесь с видами работ и ценами</div>
        </div>
            
		<div class="examples__container container">
            <div class="examples__bg"></div>

            <div class="examples__slides swiper-container exterior">
                <div class="examples__slides__wrapper swiper-wrapper">
                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/exterior/Бочка_в_частном_дворе1.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-exterior-1">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/exterior/thumbs/Бочка_в_частном_дворе1.jpg" alt="бочка в частном дворе">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__gallery">
                        	<a href="img/gallery/exterior/бочка_в_частном_дворе2.jpg" class="modal-img" data-fancybox="work-exterior-1"></a>
                        </div>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Бочка в частном дворе</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">краска в баллончиках</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value"><span>500</span> грн</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>выше среднего</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/exterior/ветеринарная_лечебница.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-exterior-2">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/exterior/thumbs/ветеринарная_лечебница.jpg" alt="Ветеринарная лечебница">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Ветеринарная лечебница</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">краска в баллончиках</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value"><span>450</span> грн</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>средняя</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/exterior/Дворик_писателя_Чуковского.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-exterior-3">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/exterior/thumbs/Дворик_писателя_Чуковского.jpg" alt="Дворик писателя Чуковского">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Дворик писателя Чуковского</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">краска в баллончиках, кисти и акрил</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value"><span>500</span> грн</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>выше среднего</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/exterior/детский_лагерь.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-exterior-4">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/exterior/thumbs/детский_лагерь.jpg" alt="Детский лагерь «Молодая гвардия»">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Детский лагерь «Молодая гвардия»</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">краска в баллончиках</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value">благотворительность, оплатили расходы</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>средняя</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/exterior/детский_скалодром.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-exterior-5">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/exterior/thumbs/детский_скалодром.jpg" alt="Детский скалодром">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Детский скалодром</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">краска в баллончиках, кисти и акрил</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value"><span>500</span> грн</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>выше среднего</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/exterior/Кафе_Свит_донатс.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-exterior-6">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/exterior/thumbs/Кафе_Свит_донатс.jpg" alt="Кафе «Свит донатс»">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Кафе «Свит донатс»</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">краска в баллончиках</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value"><span>350</span> грн</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>легкая</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/exterior/крыша_пентхауса_в_жилмассиве_Ланжерон1.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-exterior-7">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/exterior/thumbs/крыша_пентхауса_в_жилмассиве_Ланжерон1.jpg" alt="Крыша пентхауса в жилмассиве Ланжерон">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__gallery">
                        	<a href="img/gallery/exterior/крыша_пентхауса_в_жилмассиве_Ланжерон2.jpg" class="modal-img" data-fancybox="work-exterior-7"></a>
                        	<a href="img/gallery/exterior/крыша_пентхауса_в_жилмассиве_Ланжерон3.jpg" class="modal-img" data-fancybox="work-exterior-7"></a>
                        	<a href="img/gallery/exterior/крыша_пентхауса_в_жилмассиве_Ланжерон4.jpg" class="modal-img" data-fancybox="work-exterior-7"></a>
                        	<a href="img/gallery/exterior/крыша_пентхауса_в_жилмассиве_Ланжерон5.jpg" class="modal-img" data-fancybox="work-exterior-7"></a>
                        </div>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Крыша пентхауса в жилмассиве Ланжерон</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">краска в баллончиках, кисти и акрил</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value"><span>650</span> грн</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>выше среднего</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/exterior/Магазин_Ням-ням.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-exterior-8">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/exterior/thumbs/Магазин_Ням-ням.jpg" alt="Магазин «Ням-Ням»">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Магазин «Ням-Ням»</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">краска в баллончиках</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value"><span>450</span> грн</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>средняя</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/exterior/магазин_цветов_Омела.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-exterior-9">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/exterior/thumbs/магазин_цветов_Омела.jpg" alt="Магазин цветов «Омела»">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Магазин цветов «Омела»</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">краска в баллончиках</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value"><span>450</span> грн</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>средняя</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/exterior/Одесский_ботанический_сад1.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-exterior-10">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/exterior/thumbs/Одесский_ботанический_сад1.jpg" alt="Одесский ботанический сад">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__gallery">
                        	<a href="img/gallery/exterior/Одесский_ботанический_сад2.jpg" class="modal-img" data-fancybox="work-exterior-10"></a>
                        	<a href="img/gallery/exterior/Одесский_ботанический_сад3.jpg" class="modal-img" data-fancybox="work-exterior-10"></a>
                        	<a href="img/gallery/exterior/Одесский_ботанический_сад4.jpg" class="modal-img" data-fancybox="work-exterior-10"></a>
                        	<a href="img/gallery/exterior/Одесский_ботанический_сад5.jpg" class="modal-img" data-fancybox="work-exterior-10"></a>
                    	</div>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Одесский ботанический сад</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">краска в баллончиках</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value"><span>400</span> грн</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>выше среднего</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/exterior/ресторан_Рыба1.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-exterior-11">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/exterior/thumbs/ресторан_Рыба1.jpg" alt="Ресторан «Рыба»">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__gallery">
                        	<a href="img/gallery/exterior/ресторан_Рыба2.jpg" class="modal-img" data-fancybox="work-exterior-11"></a>
                    	</div>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Ресторан «Рыба»</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">краска в баллончиках</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value"><span>450</span> грн</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>выше среднего</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/exterior/Фасад_турагентства_JoinUp.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-exterior-12">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/exterior/thumbs/Фасад_турагентства_JoinUp.jpg" alt="Фасад турагентства «JoinUp»">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Фасад турагентства «JoinUp»</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">краска в баллончиках</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value"><span>400</span> грн</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>легкая</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/exterior/фасад_частного_дома.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-exterior-13">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/exterior/thumbs/фасад_частного_дома.jpg" alt="Фасад частного дома">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Фасад частного дома</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">краска в баллончиках</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value"><span>500</span> грн</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>выше среднего</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/exterior/Центр_детского_развития_Фанни_парк.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-exterior-14">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/exterior/thumbs/Центр_детского_развития_Фанни_парк.jpg" alt="Центр детского развития «Фанни парк»">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Центр детского развития «Фанни парк»</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">краска в баллончиках</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value"><span>400</span> грн</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>средняя</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/exterior/Центр_лечения_зрения_имени_Филатова.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-exterior-15">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/exterior/thumbs/Центр_лечения_зрения_имени_Филатова.jpg" alt="Центр лечения зрения имени Филатова">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Центр лечения зрения имени Филатова</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">краска в баллончиках</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value"><span>550</span> грн</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>выше среднего</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/exterior/Частный_дворик.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-exterior-16">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/exterior/thumbs/Частный_дворик.jpg" alt="Частный дворик">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Частный дворик</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">краска в баллончиках, кисти и акрил</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value">благотворительность, оплатили расходы</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>интересная</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="swiper-button-prev"><div class="swiper-button-inner"><i class="fa fa-chevron-left"></i> <span>Предыдущая работа</span></div></div>
                <div class="swiper-button-next"><div class="swiper-button-inner"><span>Следующая работа</span> <i class="fa fa-chevron-right"></i></div></div>
            </div>

            <div class="examples__slides swiper-container interior">
                <div class="examples__slides__wrapper swiper-wrapper">
                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/interior/Аквапарк_Гаваи_Аркадия.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-interior-1">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/interior/thumbs/Аквапарк_Гаваи_Аркадия.jpg" alt="Аквапарк «Гавайи», Аркадия">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Аквапарк «Гавайи», Аркадия</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">краска в баллончиках</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value"><span>450</span> грн</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>средняя</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/interior/Интерьер_кафе_Текила.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-interior-2">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/interior/thumbs/Интерьер_кафе_Текила.jpg" alt="Интерьер кафе «Текилла»">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Интерьер кафе «Текилла»</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">краска в баллончиках, губки и кисти</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value"><span>400</span> грн</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>средняя</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/interior/Комната_любителей_кальяна1.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-interior-3">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/interior/thumbs/Комната_любителей_кальяна1.jpg" alt="Комната любителей кальяна">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__gallery">
                        	<a href="img/gallery/interior/Комната_любителей_кальяна2.jpg" class="modal-img" data-fancybox="work-interior-3"></a>
                        </div>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Комната любителей кальяна</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">краска в баллончиках</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value"><span>550</span> грн</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>средняя</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/interior/Потолок_в_магазине.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-interior-4">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/interior/thumbs/Потолок_в_магазине.jpg" alt="Потолок в строительном магазине">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Потолок в строительном магазине</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">краска в баллончиках</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value"><span>300</span> грн</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>легкая</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/interior/Потолок_в_частной_кухне.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-interior-5">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/interior/thumbs/Потолок_в_частной_кухне.jpg" alt="Потолок в частной кухне">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Потолок в частной кухне</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">краска в баллончиках</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value"><span>450</span> грн</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>средняя</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/interior/Спортзал_Сто_пудов.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-interior-6">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/interior/thumbs/Спортзал_Сто_пудов.jpg" alt="Спортзал «Сто пудов»">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Спортзал «Сто пудов»</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">краска в баллончиках</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value"><span>650</span> грн</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>выше среднего</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/interior/Спортзал_Barbell_gym_1.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-interior-7">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/interior/thumbs/Спортзал_Barbell_gym_1.jpg" alt="Спортзал «Barbell gym»">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__gallery">
                        	<a href="img/gallery/interior/Спортзал_Barbell_gym_2.jpg" class="modal-img" data-fancybox="work-interior-7"></a>
                        </div>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Спортзал «Barbell gym»</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">краска в баллончиках</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value"><span>550</span> грн</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>выше среднего</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/interior/Стена_в_частном_гараже.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-interior-8">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/interior/thumbs/Стена_в_частном_гараже.jpg" alt="Стена в частном гараже">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Стена в частном гараже</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">краска в баллончиках, кисти и акрил</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value"><span>850</span> грн</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>высокая</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/interior/Фрагмент_стены_в_частном_доме.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-interior-9">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/interior/thumbs/Фрагмент_стены_в_частном_доме.jpg" alt="Фрагмент стены в частном доме">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Фрагмент стены в частном доме</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">акриловая краска, губки и кисти</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value"><span>900</span> грн</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>высокая</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/interior/Центр_детского_развития_Фанни_парк_1.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-interior-10">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/interior/thumbs/Центр_детского_развития_Фанни_парк_1.jpg" alt="Центр детского развития «Фанни парк»">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__gallery">
                        	<a href="img/gallery/interior/Центр_детского_развития_Фанни_парк_2.jpg" class="modal-img" data-fancybox="work-interior-10"></a>
                        </div>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Центр детского развития «Фанни парк»</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">краска в баллончиках</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value"><span>400</span> грн</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>средняя</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/interior/Частный_интерьер.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-interior-11">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/interior/thumbs/Частный_интерьер.jpg" alt="Частный интерьер">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Частный интерьер</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">краска в баллончиках</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value"><span>700</span> грн</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>выше среднего</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="examples__slides__item swiper-slide">
                        <a href="img/gallery/interior/Школа_иностранных_языков1.jpg" class="examples__slides__item__image_cnt modal-img" data-fancybox="work-interior-12">
                            <div class="examples__slides__item__image cover">
                                <img src="img/gallery/interior/thumbs/Школа_иностранных_языков1.jpg" alt="Школа иностранных языков">
                                <i class="fa fa-search-plus"></i>
                            </div>
                        </a>

                        <div class="examples__slides__item__gallery">
                        	<a href="img/gallery/interior/Школа_иностранных_языков2.jpg" class="modal-img" data-fancybox="work-interior-12"></a>
                        </div>

                        <div class="examples__slides__item__content">
                            <div class="examples__slides__item__content__title">Описание работы</div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Где</div>
                                    <div class="examples__slides__item__content__block__value">Школа иностранных языков</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Как</div>
                                    <div class="examples__slides__item__content__block__value">акриловая краска, губки и кисти</div>
                                </div>
                            </div>
                            <div class="col col-sm-6 col-md-12">
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">За кв/м</div>
                                    <div class="examples__slides__item__content__block__value"><span>600</span> грн</div>
                                </div>
                                <div class="examples__slides__item__content__block">
                                    <div class="examples__slides__item__content__block__title">Сложность</div>
                                    <div class="examples__slides__item__content__block__value"><span>средняя</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="swiper-button-prev"><div class="swiper-button-inner"><i class="fa fa-chevron-left"></i> <span>Предыдущая работа</span></div></div>
                <div class="swiper-button-next"><div class="swiper-button-inner"><span>Следующая работа</span> <i class="fa fa-chevron-right"></i></div></div>
            </div>
            
            <div class="examples__bottom">
                <div class="examples__tip"><b>*</b><span>цены на работы могут изменяться при полной оценке, в примерах представлены ориентировочне затраты </span></div>

                <div class="calltoform">
                    <button class="btn btn-default-line calltoform__btn"><span>ХОЧУ Себе!</span></button>
                    <div class="calltoform__desc">После заполнения формы Вам перезвонит мастер и уточнит детали</div>
                </div>

                <div class="download_pdf">
                    <a href="#" target="_blank" class="download_pdf__link">
                        <div class="download_pdf__title">Скачать полное портфолио</div>
                        <div class="download_pdf__info"><span>32 Mb (PDF)</span> <i class="fa fa-download"></i></div>
                    </a>
                </div>
            </div>
        </div>
    </section>


    <section id="reasons">
        <div class="container">
            <h2>6 причин работать с нами</h2>

            <div class="section__subtitle">то, почему Вы находитесь именно здесь</div>

            <div class="reasons__list">
                <div class="reasons__item">
                    <div class="reasons__item__image"><i class="icon-reason-1"></i></div>
                    <div class="reasons__item__content">
                        <div class="reasons__item__title"><span>Материалы</span></div>
                        <div class="reasons__item__desc">Мы работаем качественной испанской краской, сами находим поставщиков и делаем закупки</div>
                    </div>
                </div>
                <div class="reasons__item">
                    <div class="reasons__item__image"><i class="icon-reason-2"></i></div>
                    <div class="reasons__item__content">
                        <div class="reasons__item__title"><span>Результат</span></div>
                        <div class="reasons__item__desc">Вы получаете водостойкий, насыщенный рисунок, исполненный индивидуально по Вашей идее</div>
                    </div>
                </div>
                <div class="reasons__item">
                    <div class="reasons__item__image"><i class="icon-reason-3"></i></div>
                    <div class="reasons__item__content">
                        <div class="reasons__item__title"><span>Консультация</span></div>
                        <div class="reasons__item__desc">Бесплатная консультация и составление эскиза на компьютере</div>
                    </div>
                </div>
                <div class="reasons__item">
                    <div class="reasons__item__image"><i class="icon-reason-4"></i></div>
                    <div class="reasons__item__content">
                        <div class="reasons__item__title"><span>Оплата одним чеком</span></div>
                        <div class="reasons__item__desc">Без разделения: «за разработку эскиза», «за материалы», «за работу»</div>
                    </div>
                </div>
                <div class="reasons__item">
                    <div class="reasons__item__image"><i class="icon-reason-5"></i></div>
                    <div class="reasons__item__content">
                        <div class="reasons__item__title"><span>Без подрядчиков</span></div>
                        <div class="reasons__item__desc">Мы не посредники, а прямые исполнители работы, поэтому нет никакой переплаты агентам</div>
                    </div>
                </div>
                <div class="reasons__item">
                    <div class="reasons__item__image"><i class="icon-reason-6"></i></div>
                    <div class="reasons__item__content">
                        <div class="reasons__item__title"><span>Опытные мастера</span></div>
                        <div class="reasons__item__desc">Работа выполняется художниками с более чем десятилетним опытом в этой сфере деятельности</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    

    <section id="faq_reviews">
        <div class="container">
            <div class="row">
                <div class="col col-lg-6">
                    <div class="faq">
                        <h2>вопрос - ответ</h2>

                        <div class="panel-group faq__accordion" id="faq__accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel faq__accordion__item">
                                <div class="faq__accordion__item__head" role="tab" id="faq__accordion__item__head-1">
                                    <a role="button" data-toggle="collapse" data-parent="#faq__accordion" href="#faq__accordion__item__content-1" aria-controls="faq__accordion__item__content-1" aria-expanded="true">Есть ли у Вас художественное образование?</a>
                                </div>
                                <div id="faq__accordion__item__content-1" class="faq__accordion__item__content collapse in" role="tabpanel" aria-labelledby="faq__accordion__item__head-1">
                                    <div class="faq__accordion__item__content__body">
                                        В нашей команде трудится несколько человек. Кто-то закончил художественное учебное заведение, кто-то развивался самостоятельно, обучаясь на мастер классах и собственном опыте. В любом случае, заказывая роспись у нас, Вы заказываете ее у профессионалов, работающих конкретно в этой области более 10 лет, людей, любящих свое дело и посвящающих развитию значительную часть своего времени.
                                    </div>
                                </div>
                            </div>

                            <div class="panel faq__accordion__item">
                                <div class="faq__accordion__item__head" role="tab" id="faq__accordion__item__head-2">
                                    <a role="button" data-toggle="collapse" data-parent="#faq__accordion" href="#faq__accordion__item__content-2" aria-controls="faq__accordion__item__content-2" aria-expanded="false">От чего зависит цена за работу?</a>
                                </div>

                                <div id="faq__accordion__item__content-2" class="faq__accordion__item__content collapse" role="tabpanel" aria-labelledby="faq__accordion__item__head-2">
                                    <div class="faq__accordion__item__content__body">
                                        Наши цены достаточно гибкие. Для каждого клиента цена просчитывается (а не берется из потолка), исходя из сложности работы, детализации, количества цветов/оттенков, желаемой стилистики, объема работ. Например, средняя цена баллончика качественной краски равна 100 грн., а приблизительный расход такой краски - один баллончик на квадратный метр. Вот и получается, что только на краску уходит 100 грн/кв.м, не говоря уже о других расходах и потраченном времени самого мастера.
                                    </div>
                                </div>
                            </div>

                            <div class="panel faq__accordion__item">
                                <div class="faq__accordion__item__head" role="tab" id="faq__accordion__item__head-3">
                                    <a role="button" data-toggle="collapse" data-parent="#faq__accordion" href="#faq__accordion__item__content-3" aria-controls="faq__accordion__item__content-3" aria-expanded="false">Почему я должен платить таке деньги за пару дней работы?</a>
                                </div>

                                <div id="faq__accordion__item__content-3" class="faq__accordion__item__content collapse" role="tabpanel" aria-labelledby="faq__accordion__item__head-3">
                                    <div class="faq__accordion__item__content__body">
                                        Да, чтобы нарисовать привлекательную качественную работу у нас иногда уходит всего пара дней, но каждый из нас потратил минимум по десять лет, чтобы создавать работу такого качества за пару дней.
                                    </div>
                                </div>
                            </div>

                            <div class="panel faq__accordion__item">
                                <div class="faq__accordion__item__head" role="tab" id="faq__accordion__item__head-4">
                                    <a role="button" data-toggle="collapse" data-parent="#faq__accordion" href="#faq__accordion__item__content-4" aria-controls="faq__accordion__item__content-4" aria-expanded="false">Какой должна быть стена под роспись?</a>
                                </div>

                                <div id="faq__accordion__item__content-4" class="faq__accordion__item__content collapse" role="tabpanel" aria-labelledby="faq__accordion__item__head-4">
                                    <div class="faq__accordion__item__content__body">
                                        Главное требование к поверхности - она не должна сыпаться. Если Вы проводите по стене рукой, и на руке остается пыль, побелка, мусор, мелкие камешки - стену обязательно нужно прогрунтовать. Все остальное - на усмотрение. Стена не обязательно должна быть идеально ровной, чаще наоборот - клиенты заказывают у нас роспись, чтобы скрыть недостатки и неровности стены. Если у Вас есть сомнения - просто вызовите нашего мастера, он бесплатно приедет и проконсультирует.
                                    </div>
                                </div>
                            </div>

                            <div class="panel faq__accordion__item">
                                <div class="faq__accordion__item__head" role="tab" id="faq__accordion__item__head-5">
                                    <a role="button" data-toggle="collapse" data-parent="#faq__accordion" href="#faq__accordion__item__content-5" aria-controls="faq__accordion__item__content-5" aria-expanded="false">Сколько веремени держится краска, выгорает ли она?</a>
                                </div>

                                <div id="faq__accordion__item__content-5" class="faq__accordion__item__content collapse" role="tabpanel" aria-labelledby="faq__accordion__item__head-5">
                                    <div class="faq__accordion__item__content__body">
                                        Мы используем самую лучшую краску, которую можем найти. Для нас, как и для клиента важно, чтобы результат труда продержался в хорошем состоянии как можно дольше. Для уличных работ мы используем специально разработанную граффити-краску в баллончиках, изготовленную на заводах Испании (краски mtn94, Kobra) и Германии ( краски Montana Black и Montana Gold). Нарисованные такой краской работы держатся много лет даже на солнечной стороне, им не страшны погодные условия, дожди и морозы. Главное условие - хорошо подготовленная поверхность.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col col-lg-6">
                    <div class="reviews">
                        <h2>отзывы</h2>

                        <div class="reviews__slides swiper-container">
                            <div class="reviews__slides__wrapper swiper-wrapper">
                                <div class="reviews__slides__item swiper-slide">
                                    <a href="https://vk.com/id10069056" target="_blank" class="reviews__slides__item__top">
                                        <div class="reviews__slides__item__image cover">
                                            <img src="img/pictures/Эрнест_Зайченко.jpg" alt="Эрнест Зайченко">
                                            <div class="reviews__slides__item__social reviews__slides__item__social-vk"></div>
                                        </div>
                                        <div class="reviews__slides__item__name">Эрнест Зайченко</div>
                                    </a>
    
                                    <div class="reviews__slides__item__bottom">
                                        <div class="reviews__slides__item__desc">
                                            <p>Заказал у ребят граффити на стенах на площади 80 квадратов, работа была выполнена в кратчайшие сроки, что удивило для такого объема. совет для заказчиков - заранее продумайте что точно вам надо, чтобы утверждение эскизов с художниками не происходило в авральном режиме)) потому как ребята работают быстрее , чем вы придумываете , что вам надо) совет Алексею- перед тем как браться за работу- утвердите эскиз на всю работу, это сэкономит вам время и заказчик будет более доволен</p>

                                            <p>p.s. работа выполнена отлично! я очень доволен, художники позитивны, отзывчивы к любым замечаниям и предложениям . скорость и качество - вот принцип работы Алексея и Тани:)</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="reviews__slides__item swiper-slide">
                                    <a href="https://vk.com/id60864008" target="_blank" class="reviews__slides__item__top">
                                        <div class="reviews__slides__item__image cover">
                                           <img src="img/pictures/Артем_Пименов.jpg" alt="Артем Пименов">
                                            <div class="reviews__slides__item__social reviews__slides__item__social-vk"></div>
                                        </div>
                                        <div class="reviews__slides__item__name">Артем Пименов</div>
                                    </a>
    
                                    <div class="reviews__slides__item__bottom">
                                        <div class="reviews__slides__item__desc">
                                            <p>Креативный коллектив с небанальным почерком исполнения!!!</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="reviews__slides__item swiper-slide">
                                    <a href="https://vk.com/id295854541" target="_blank" class="reviews__slides__item__top">
                                        <div class="reviews__slides__item__image cover">
                                           <img src="img/pictures/Каролина_Феличе.jpg" alt="Каролина Феличе">
                                            <div class="reviews__slides__item__social reviews__slides__item__social-vk"></div>
                                        </div>
                                        <div class="reviews__slides__item__name">Каролина Феличе</div>
                                    </a>
    
                                    <div class="reviews__slides__item__bottom">
                                        <div class="reviews__slides__item__desc">
                                            <p>Спасибо ребятам за флаг) Очень ответственные, честные, понимающие люди и Руки Золотые! Я желаю, Вам достичь больших успехов! </p>
                                            <p>Уверенна, что Ваши имена будут известны во всех странах Мира!</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="reviews__slides__item swiper-slide">
                                    <a href="https://vk.com/k_r_a_i_t" target="_blank" class="reviews__slides__item__top">
                                        <div class="reviews__slides__item__image cover">
                                           <img src="img/pictures/Эдуард_Гончаров.jpg" alt="Эдуард Гончаров">
                                            <div class="reviews__slides__item__social reviews__slides__item__social-vk"></div>
                                        </div>
                                        <div class="reviews__slides__item__name">Эдуард Гончаров</div>
                                    </a>
    
                                    <div class="reviews__slides__item__bottom">
                                        <div class="reviews__slides__item__desc">
                                            <p>Работой очень доволен, результат оказался намного лучшим, чем мог себе представить!))))</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="reviews__slides__item swiper-slide">
                                    <a href="https://vk.com/korobkov71" target="_blank" class="reviews__slides__item__top">
                                        <div class="reviews__slides__item__image cover">
                                           <img src="img/pictures/Олего_Коробков.jpg" alt="Олег Коробков">
                                            <div class="reviews__slides__item__social reviews__slides__item__social-vk"></div>
                                        </div>
                                        <div class="reviews__slides__item__name">Олег Коробков</div>
                                    </a>
    
                                    <div class="reviews__slides__item__bottom">
                                        <div class="reviews__slides__item__desc">
                                            <p>Решился попробовать заказать портрет и был приятно удивлен, как индивидуальному подходу к клиенту и моим требованиям, так и правильности, а главное точности исполнения в реальности... Огромное спасибо, ребята. Просто удивили!... Желаю Вам творческого долголетия при максимуме разнообразия! И конечно надеюсь на дальнейшее сотрудничество, а главное реально всем рекомендую!!!... Еще раз спасибо…</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="reviews__slides__item swiper-slide">
                                    <a href="https://vk.com/id11986835" target="_blank" class="reviews__slides__item__top">
                                        <div class="reviews__slides__item__image cover">
                                           <img src="img/pictures/Артем_Быстрицкий.jpg" alt="Артем Быстрицкий">
                                            <div class="reviews__slides__item__social reviews__slides__item__social-vk"></div>
                                        </div>
                                        <div class="reviews__slides__item__name">Артем Быстрицкий</div>
                                    </a>
    
                                    <div class="reviews__slides__item__bottom">
                                        <div class="reviews__slides__item__desc">
                                            <p>Благодарю ребят за работу по росписи стен пиццерии. Сначала выбрали и согласовали дизайн первой пиццерии. Сроки немного растянули, но результатом остались довольны. Через год открывали еще одно заведение, заметил, что уровень мастерства заметно вырос. Результат превзошел ожидания. Рекомендую всем, а также друзьям и знакомым.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="swiper-pagination"></div>

                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="contactform">
        <div class="container">
            <h2>Оставьте заявку</h2>

            <div class="contactform__form">
                <form class="m-x-auto" name="form">
                    <div class="form-group">
                        <label for="user_name" class="form-label">Имя, фамилия</label>
                        <i class="fa fa-user"></i>
                        <input type="text" name="user_name" id="user_name" class="form-control" tabindex="1" required>
                    </div>

                    <div class="form-group">
                        <label for="user_phone" class="form-label">Телефон</label>
                        <i class="fa fa-phone"></i>
                        <input type="tel" name="user_phone" id="user_phone" class="form-control" tabindex="2" required>
                    </div>

                    <div class="form-group form-group-text">
                        <label for="user_message" class="form-label">Ваш комментарий</label>
                        <i class="fa fa-pencil"></i>
                        <textarea id="user_message" name="user_message" class="form-control" rows="8" tabindex="3"></textarea>
                    </div>
                    
                    <div class="contactform__bottom">
                        <div class="contactform__file_cnt">
                            <div class="form-group contactform__file">
                                <label class="form-label contactform__file__add" tabindex="4">
                                    <span>ПРИКРЕПИТЕ ФОТО</span><i class="fa fa-paperclip"></i>
                                </label>

                                <input id="user_file" type="file" name="user_file" class="form-control" accept="image/*">

                                <div class="contactform__file__del">
                                    <div class="contactform__file__del__info">
                                        <span></span><i class="fa fa-times"></i>
                                    </div>
                                </div>

                                <div class="contactform__file__loader"><i class="fa fa-spinner"></i></div>
                            </div>
                        </div>
                        
                        <div class="contactform__cnt-btns">
                            <div class="contactform__cnt-btn">
                                <button class="contactform__btn-submit btn btn-default" type="submit" tabindex="5"><span>ОТПРАВИТЬ</span></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
	
	
	<section class="changetype exterior">
		<h2>Интерьеры</h2>
		
		<div class="changetype__block">
			<div class="changetype__block__inner">
	            <div class="changetype__block__info">
	                <div class="changetype__block__content">
	                    <span>Рисунок в интерьере будет заполнять пустоту и радовать Ваших гостей.</span>
	                    <div class="changetype__block__btn btn btn-blue js-changetype_btn" data-section="interior">детали</div>
	                </div>
	            </div>
			</div>
		</div>
	</section>
	
	
	<section class="changetype interior">
		<h2>Экстерьеры</h2>
		
		<div class="changetype__block">
			<div class="changetype__block__inner">
	            <div class="changetype__block__info">
	                <div class="changetype__block__content">
	                    <span>Мы можем изменить Ваш фасад так, что он будет не только украшать, но и продавать.</span>
	                    <div class="changetype__block__btn btn btn-blue js-changetype_btn" data-section="exterior">детали</div>
	                </div>
	            </div>
			</div>
		</div>
	</section>


    <footer class="footer">
        <div class="container">
            <div class="footer__contacts">
                <div class="footer__phones">
                    <a href="callto:096 279-60-05" class="footer__phones__item">096 279-60-05</a>
                    <a href="callto:099 676-21-22" class="footer__phones__item">099 676-21-22</a>
                </div>

                <div class="footer__person">Алексей</div>
            </div>

            <div class="social social-footer">
                <div class="social_inner">
                    <a href="http://vk.com/clubfairyart" class="social__item" target="_blank"><i class="icon icon-social-vk"></i></a>
                    <a href="https://www.instagram.com/graffiti_artbaka/" class="social__item" target="_blank"><i class="icon icon-social-ig"></i></a>
                    <a href="https://www.facebook.com/people/%D0%90%D0%BB%D0%B5%D0%BA%D1%81%D0%B5%D0%B9-%D0%91%D0%B0%D0%BA%D0%B0/100005350004824" class="social__item" target="_blank"><i class="icon icon-social-fb"></i></a>
                </div>
            </div>
        </div>
    </footer>


    <div class="parallax_cnt">
        <div class="parallax-1 rellax" data-rellax-speed="2"></div>
        <div class="parallax-2 rellax" data-rellax-speed="3"></div>
        <div class="parallax-3 rellax" data-rellax-speed="5"></div>
        <div class="parallax-4 rellax" data-rellax-speed="3"></div>
        <div class="parallax-5 rellax" data-rellax-speed="5"></div>
        <div class="parallax-6 rellax" data-rellax-speed="3"></div>
        <div class="parallax-7 rellax" data-rellax-speed="4"></div>
    </div>
</div>


<div class="contactform modal_contactform">
	<div class="modal_contactform__overlay modal_contactform_close" title="Закрыть"></div>

	<div class="modal_contactform_inner">
		<div class="modal_contactform__btn-close modal_contactform_close" title="Закрыть"><i class="fa fa-close"></i></div>

	    <div class="container">
	        <h2>Оставьте заявку</h2>

	        <div class="contactform__form">
	            <form class="m-x-auto" name="form">
	                <div class="form-group">
	                    <label for="user_name-modal" class="form-label">Имя, фамилия</label>
	                    <i class="fa fa-user"></i>
	                    <input type="text" name="user_name" id="user_name-modal" class="form-control" tabindex="1" required>
	                </div>

	                <div class="form-group">
	                    <label for="user_phone-modal" class="form-label">Телефон</label>
	                    <i class="fa fa-phone"></i>
	                    <input type="tel" name="user_phone" id="user_phone-modal" class="form-control" tabindex="2" required>
	                </div>

	                <div class="form-group form-group-text">
	                    <label for="user_message-modal" class="form-label">Ваш комментарий</label>
	                    <i class="fa fa-pencil"></i>
	                    <textarea id="user_message-modal" name="user_message" class="form-control" rows="8" tabindex="3"></textarea>
	                </div>
	                
	                <div class="contactform__bottom">
	                    <div class="contactform__file_cnt">
	                        <div class="form-group contactform__file">
	                            <label class="form-label contactform__file__add" tabindex="4">
	                                <span>ПРИКРЕПИТЕ ФОТО</span><i class="fa fa-paperclip"></i>
	                            </label>

	                            <input id="user_file-modal" type="file" name="user_file" class="form-control" accept="image/*">

	                            <div class="contactform__file__del">
	                                <div class="contactform__file__del__info">
	                                    <span></span><i class="fa fa-times"></i>
	                                </div>
	                            </div>

	                            <div class="contactform__file__loader"><i class="fa fa-spinner"></i></div>
	                        </div>
	                    </div>
	                    
	                    <div class="contactform__cnt-btns">
	                        <div class="contactform__cnt-btn">
	                            <button class="contactform__btn-submit btn btn-default" type="submit" tabindex="5"><span>ОТПРАВИТЬ</span></button>
	                        </div>
	                    </div>
	                </div>
	            </form>
	        </div>
	    </div>
    </div>
</div>
</body>

<!-- build:jslibs -->
<script src="js/libs.js" type="text/javascript"></script>
<!-- endbuild -->
<!-- build:jscommon -->
<script src="js/common.js" type="text/javascript"></script>
<!-- endbuild -->
</html>