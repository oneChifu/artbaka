<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="format-detection" content="telephone=no">
	<title>Письмо от Artbaka.com.ua</title>

	<link href="../../css/mail.css" rel="stylesheet" />
</head>

<body style="margin:0; padding:0;" bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
	<tr>
		<td class="main" align="center" valign="top" bgcolor="#ffffff" style="background-color: #ffffff;">
			<div class="head">
				<div class="title">Ви отправили письмо с сайта<br><a href="http://artbaka.com.ua" target="_blank">artbaka.com.ua</a></div>
				<div class="subtitle">Ми свяжемся с Вами в ближайшее время ;)</div>
			</div>

			<table class="content">
				<tr>
					<td class="content__title">Вас зовут:</td>
					<td class="content__value">Павло Лаврусь</td>
				</tr>
				<tr>
					<td class="content__title">Ваш номер телефона:</td>
					<td class="content__value"><a href="callto:(096) 766-39-74">(096) 766-39-74</a></td>
				</tr>
				<tr>
					<td class="content__title">Также Ви написали:</td>
					<td class="content__value">Далеко-далеко за словесными горами в стране, гласных и согласных живут рыбные тексты. Предупреждал переписывается своих силуэт, предупредила всеми текстов он буквоград переписали буквенных рукопись правилами курсивных бросил вершину путь, дорогу, инициал даже.</td>
				</tr>
			</table>

			<div class="social">
                <a href="#" target="_blank" class="social__item" title="Skype"><img src="../../img/icons/icon-social-skype-hover.png" alt="skype"></a>
                <a href="#" target="_blank" class="social__item" title="Instagram"><img src="../../img/icons/icon-social-ig-hover.png" alt="instagram"></a>
                <a href="#" target="_blank" class="social__item" title="Facebook"><img src="../../img/icons/icon-social-fb-hover.png" alt="facebook"></a>
            </div>

            <div class="footer">
            	Наши телефоны: <br><a href="callto:+380962796005">096 279-60-05</a><br><a href="callto:+380996762122">099 676-21-22</a>
            </div>
		</td>
	</tr>
</table>
</body>
</html>