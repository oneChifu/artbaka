<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="format-detection" content="telephone=no">
	<title>Письмо от Artbaka.com.ua</title>

	
</head>

<body style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-family: Arial, Helvetica, sans-serif; margin: 0; padding: 0;" bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#ffffff" style="border-spacing: 0; margin: auto; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
	<tr>
		<td class="main" align="center" valign="top" bgcolor="#ffffff" style="background-color: #ffffff; border: 3px solid #d7d7d7; border-collapse: collapse;">
			<div class="head" style="background: #0069b4; color: #ffffff; padding: 20px 15px;">
				<div class="title" style="color: #ffffff; font-size: 24px; font-weight: 700; line-height: 30px; margin-bottom: 10px;">Ви отправили письмо с сайта<br><a href="http://artbaka.com.ua" target="_blank" style="color: #ffffff; text-decoration: none;">artbaka.com.ua</a></div>
				<div class="subtitle" style="color: #ffffff; font-size: 16px; font-weight: 400; line-height: 20px;">Ми свяжемся с Вами в ближайшее время ;)</div>
			</div>

			<table class="content" style="border-spacing: 0; margin: 20px 0; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
				<tr>
					<td class="content__title" style="border-collapse: collapse; color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: 700; line-height: 18px; padding: 6px 15px; padding-top: 8px; text-align: right; vertical-align: top; width: 150px;">Вас зовут:</td>
					<td class="content__value" style="border-collapse: collapse; color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding: 6px 15px; text-align: left; vertical-align: top;">Павло Лаврусь</td>
				</tr>
				<tr>
					<td class="content__title" style="border-collapse: collapse; color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: 700; line-height: 18px; padding: 6px 15px; padding-top: 8px; text-align: right; vertical-align: top; width: 150px;">Ваш номер телефона:</td>
					<td class="content__value" style="border-collapse: collapse; color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding: 6px 15px; text-align: left; vertical-align: top;"><a href="callto:(096) 766-39-74" style="color: #0069b4; text-decoration: none;">(096) 766-39-74</a></td>
				</tr>
				<tr>
					<td class="content__title" style="border-collapse: collapse; color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: 700; line-height: 18px; padding: 6px 15px; padding-top: 8px; text-align: right; vertical-align: top; width: 150px;">Также Ви написали:</td>
					<td class="content__value" style="border-collapse: collapse; color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding: 6px 15px; text-align: left; vertical-align: top;">Далеко-далеко за словесными горами в стране, гласных и согласных живут рыбные тексты. Предупреждал переписывается своих силуэт, предупредила всеми текстов он буквоград переписали буквенных рукопись правилами курсивных бросил вершину путь, дорогу, инициал даже.</td>
				</tr>
			</table>

			<div class="social" style="margin: 30px 0 10px 0;">
                <a href="#" target="_blank" class="social__item" title="Skype" style="display: inline-block; height: 42px; margin: 0 10px; width: 42px;"><img src="../../img/icons/icon-social-skype-hover.png" alt="skype" style="-ms-interpolation-mode: bicubic; display: block; width: 100%;"></a>
                <a href="#" target="_blank" class="social__item" title="Instagram" style="display: inline-block; height: 42px; margin: 0 10px; width: 42px;"><img src="../../img/icons/icon-social-ig-hover.png" alt="instagram" style="-ms-interpolation-mode: bicubic; display: block; width: 100%;"></a>
                <a href="#" target="_blank" class="social__item" title="Facebook" style="display: inline-block; height: 42px; margin: 0 10px; width: 42px;"><img src="../../img/icons/icon-social-fb-hover.png" alt="facebook" style="-ms-interpolation-mode: bicubic; display: block; width: 100%;"></a>
            </div>

            <div class="footer" style="background: #fa2a2a; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 16px; font-weight: 700; line-height: 24px; padding: 30px 15px;">
            	Наши телефоны: <br><a href="callto:+380962796005" style="color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 16px; font-weight: 700; line-height: 24px; text-decoration: underline;">096 279-60-05</a><br><a href="callto:+380996762122" style="color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 16px; font-weight: 700; line-height: 24px; text-decoration: underline;">099 676-21-22</a>
            </div>
		</td>
	</tr>
</table>
</body>
</html>