var gulp = require('gulp'),
	del = require('del'),
	cache = require('cache'),
	sass = require('gulp-sass'),
	compass = require('gulp-compass'),
	browserSync = require('browser-sync'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	sourcemaps = require('gulp-sourcemaps'),
	postcss = require('gulp-postcss'),
	autoprefixer = require('autoprefixer'),
	gulpAutoprefixer = require('gulp-autoprefixer'),
	postcssReporter = require('postcss-reporter'),
	postcssScss = require('postcss-scss'),
	postcssDiscardComments = require('postcss-discard-comments'),
	cssnano = require('cssnano'),
	imagemin = require('gulp-imagemin'),
	imageop = require('gulp-image-optimization'),
	pngquant = require('gulp-pngquant'),
	fontmin = require('gulp-fontmin'),
	fileinclude = require('gulp-file-include'),
	inlineCss = require('gulp-inline-css'),
	rename = require('gulp-rename'),
	modernizr = require('gulp-modernizr'),
	uglifycss = require('gulp-uglifycss'),
	uncss = require('gulp-uncss'),
	htmlreplace = require('gulp-html-replace');



gulp.task('sass', function() {
	var plugins = [
        autoprefixer({
        	browsers: ['last 15 versions', 'ie 6-8', '> 1%'],
        	grid: true
        }),
    ];

    function swallowError(error) {
		// If you want details of the error in the console
		// console.log(error.toString())
		this.emit('end');
	}

	// return gulp.src('src/scss/**/*.scss')
	return gulp.src('src/scss/**/*.scss')
		.pipe(compass({
			css: 'src/css',
			sass: 'src/scss',
			image: 'src/img'
	    }))
	    .on('error', function(err) {
			// Would like to catch the error here 
			this.emit('end');
		})
		.pipe(postcss(plugins))
		.pipe(gulp.dest('src/css'))
		.pipe(browserSync.reload({stream: true}))
});


gulp.task('scripts', function() {
	return gulp.src([
		'src/libs/modernizr/build/modernizr-custom.js',
		'src/libs/jquery/dist/jquery.min.js',
		'src/libs/bootstrap/dist/js/bootstrap.min.js',
		'src/libs/jquery-touchswipe/jquery.touchSwipe.min.js',
		'src/libs/swiper/dist/js/swiper.jquery.min.js',
		'src/libs/jquery-validation/dist/jquery.validate.min.js',
		'src/libs/jquery-validation/dist/additional-methods.min.js',
		'src/libs/jquery.maskedinput/dist/jquery.maskedinput.min.js',
		'src/libs/sweetalert2/dist/sweetalert2.min.js',
		'src/libs/fancybox/dist/jquery.fancybox.min.js',
		'src/libs/rellax/rellax.min.js',
	])
	.pipe(concat('libs.js')) // Собираем их в кучу в новом файле libs.min.js
	// .pipe(uglify()) // Сжимаем JS файл
	.pipe(gulp.dest('src/js')); // Выгружаем в папку app/js
});


gulp.task('styles', function() {
	var plugins = [
		// postcssDiscardComments({ // Remove all comments in css files
		// 	removeAll: true
		// }),
        // cssnano()
    ];

	return gulp.src([
			'src/libs/sanitize-css/sanitize.css',
			'src/libs/font-awesome/css/font-awesome.min.css',
			'src/libs/bootstrap/dist/css/bootstrap.min.css',
			'src/libs/swiper/dist/css/swiper.min.css',
			'src/libs/sweetalert2/dist/sweetalert2.min.css',
			'src/libs/fancybox/dist/jquery.fancybox.min.css',
		])
		.pipe(concat('libs.css'))
		.pipe(postcss([
			cssnano()
		]))
		.pipe(uglifycss({
			"uglyComments": true
		}))
		.pipe(gulp.dest('src/css'));
});


// Запуск серверу
gulp.task('browser-sync', function() {
	browserSync({
		proxy: 'artbaka',
		notify: false
	});
});


gulp.task('watch', ['browser-sync', 'sass'], function() {
	gulp.watch('src/scss/**/*.scss', ['sass']);
	// gulp.watch('src/**/*.html', browserSync.reload);
	gulp.watch('src/*.php', browserSync.reload);
	gulp.watch('src/js/**/*.js', browserSync.reload);
});


gulp.task('clean', function() {
	return del.sync([
		'dist/*', 
		'!dist/.git', 
		'!dist/favicon.ico'
	]); // Удаляем папку dist перед сборкой
});


gulp.task('img', function() {
	return gulp.src('src/img/**/*') // Берем все изображения из app
	.pipe(imagemin([
			imagemin.gifsicle({interlaced: true}),
			imagemin.jpegtran({progressive: true}),
			imagemin.optipng({optimizationLevel: 3}),
			imagemin.svgo({plugins: [{removeViewBox: true}]})
		],
		{  // Сжимаем их с наилучшими настройками с учетом кеширования
			verbose: true,
		}))
	.pipe(gulp.dest('dist/img')); // Выгружаем на продакшен
});


gulp.task('mailimg', function() {
	return gulp.src('src/mail/templates/img/**/*')
	.pipe(imagemin({
		interlaced: true,
		progressive: true,
		svgoPlugins: [{removeViewBox: false}],
		use: [pngquant()]
	}))
	.pipe(gulp.dest('dist/mail/templates/img'));
});


gulp.task('mail', ['mailimg'], function() {
    return gulp.src(['src/mail/templates/*.html', 'src/mail/templates/*.php'])
    .pipe(inlineCss())
    .pipe(gulp.dest('dist/mail/templates/'));
});


gulp.task('iconfont', function() {
	var runTimestamp = Math.round(Date.now()/1000);

	return gulp.src(['src/fonts/src/*.otf'])
	.pipe(iconfont({
		fontName: 'Gilroy', // required 
		prependUnicode: true, // recommended option 
		formats: ['ttf', 'eot', 'woff', 'svg'], // default, 'woff2' and 'svg' are available 
		timestamp: runTimestamp, // recommended to get consistent builds when watching files 
	}))
	.on('glyphs', function(glyphs, options) {
		// CSS templating, e.g. 
		console.log(glyphs, options);
	})
	.pipe(gulp.dest('src/fonts/'));
});


gulp.task('fontmin', function () {
    return gulp.src(['src/fonts/**/*.eot', 'src/fonts/**/*.woff', 'src/fonts/**/*.woff2', 'src/fonts/**/*.svg', 'src/fonts/**/*.ttf'])
    .pipe(fontmin())
    .pipe(gulp.dest('dist/fonts'));
});


gulp.task('build', ['clean', 'img', 'sass', 'scripts', 'styles', 'fontmin', 'mail'], function() {
	var buildCssLibs = gulp.src('src/css/libs.css')
	.pipe(rename({
    	suffix: '.min'
    }))
    .pipe(sourcemaps.write('.'))
	.pipe(gulp.dest('dist/css'))

	var buildCssMain = gulp.src('src/css/main.css')
	// .pipe(postcss([
	// 	postcssDiscardComments({
	// 		removeAll: true
	// 	}),
        // cssnano()
    // ]))
    // .pipe(postcssDiscardComments({
    // 	removeAll: true
    // }))
    .pipe(uglifycss({
		"uglyComments": true
	}))
    .pipe(rename({
    	suffix: '.min'
    }))
    .pipe(sourcemaps.write('.'))
	.pipe(gulp.dest('dist/css'))

	var buildJs = gulp.src('src/js/**/*') // Переносим скрипты в продакшен
	.pipe(uglify()) // Сжимаем JS файл
	.pipe(rename({
    	suffix: '.min'
    }))
	.pipe(gulp.dest('dist/js'))

	var buildHtml = gulp.src('src/*.html') // Переносим HTML в продакшен
	.pipe(gulp.dest('dist'));

	var buildPhp = gulp.src('src/*.php') // Переносим HTML в продакшен
    .pipe(htmlreplace({
        'csslibs': 'css/libs.min.css',
        'cssmain': 'css/main.min.css',
        'jslibs': 'js/libs.min.js',
        'jscommon': 'js/common.min.js',
    }))
	.pipe(gulp.dest('dist'));

	var buildMail = gulp.src('src/mail/*.php') // Переносим HTML в продакшен
	.pipe(gulp.dest('dist/mail'));
});


gulp.task('clear', function (callback) {
	return cache.clearAll();
})


gulp.task('cssmainnano', function (callback) {
	return gulp.src('src/css/main.css')
	.pipe(postcss())
    .pipe(uglifycss({
		"uglyComments": true
	}))
    .pipe(rename({
    	suffix: '.min'
    }))
	.pipe(gulp.dest('src/css'))
})






// gulp.task('modernizr', function() {
// 	return gulp.src('src/libs/modernizr/*.js')
// 	.pipe(modernizr('modernizr.min.js', {
//     	'options': [],
// 		'feature-detects': [],
// 	}))
// 	// .pipe(uglify())
// 	.pipe(gulp.dest("src/libs/modernizr/build/"))
// });



gulp.task('default', ['watch']);

