/* author: Pavlo Lavrus */
/* site: bytadat.com */


/* First screen */
function firstScreen() {
	var Blocks = $('.head__block, .head__blocks'),
		Body = $('body');

	if ( Modernizr.mq('(max-width: 767px)') ) {
		Blocks.height($(window).height());

		if ( Body.hasClass('is-first') ) {
			$('#wrapper').height($(window).height()*2);
		}
	}

	if ( Modernizr.mq('(min-width: 768px)') && Modernizr.mq('(max-width: 991px)') ) {
		Blocks.height($(window).height() / 2);
	}

	if ( Modernizr.mq('(min-width: 768px)') ) {
		if ( Body.hasClass('is-first') ) {
			$('#wrapper').height($(window).height());
		}
	}

	if ( Modernizr.mq('(min-width: 992px)') ) {
		Blocks.height($(window).height());
	}

	if ( Modernizr.mq('(min-width: 1200px)') ) {
		$('.parallax_cnt').css({
			height: function() {
				return $(document).height()-$(window).height()+'px';
			}
		});
	}
}

$(window).on('load', function() {
	$('body').addClass('is-loaded is-first');
	firstScreen();
});

$(window).resize(firstScreen);

$(document).on('click', '.js-changetype_btn', function(e) {
	$('body').removeClass('is-exterior is-interior is-first').addClass('is-'+$(this).data('section'));

	$('#wrapper').height('auto')

	$("html, body").stop(true).animate({
		scrollTop: parseInt($('#steps').offset().top)-20
	}, 500, 'swing');

	stepTimeout();
})


/* Steps section */
function steps() {
	$(window).on('load', function() {
		$('.steps__item-icon').first().addClass('active').next().addClass('right').next().addClass('is-hide');
		stepDesc($('.steps__item-desc').first().addClass('active').data('step'));
	});

	this.stepNext = function() {
		var ItemActive = $('.steps__item-icon.active');
		if ( ItemActive.next().length > 0 ) {
			ItemActive.next().addClass('active').removeClass('right').next().addClass('right').removeClass('is-hide').next().addClass('is-hide');
			ItemActive.removeClass('active').addClass('left').prev().addClass('is-hide');

			stepDesc(ItemActive.next().data('step'));
		}
	}

	this.stepPrev = function() {
		var ItemActive = $('.steps__item-icon.active');
		if ( ItemActive.prev().length > 0 ) {
			ItemActive.prev().addClass('active').removeClass('left').prev().addClass('left').removeClass('is-hide').prev().addClass('is-hide');
			ItemActive.removeClass('active').addClass('right').next().addClass('is-hide');

			stepDesc(ItemActive.prev().data('step'));
		}
	}

	this.stepCheck = function(Item) {
		var ItemStep = $(Item).data('step');

		$('.steps__item-icon').removeClass('active');
		Item.addClass('active').removeClass('is-hide, right, left');
		Item.prev().addClass('left').removeClass('is-hide').prev().addClass('is-hide');
		Item.next().addClass('right').removeClass('is-hide').next().addClass('is-hide');
		stepDesc(ItemStep);
	}

	var stepDesc = function(Step) {
		var ItemDescActive = $('.steps__item-desc[data-step='+Step+']');

		$('.steps__item-desc').removeClass('active');
		ItemDescActive.addClass('active');
		$('.steps__descs').css({
			height: function() {
				return ItemDescActive.outerHeight();
			}
		});
	}

	var StepTimeout = 0;
	this.stepTimeout = function() {
		StepTimeout = setTimeout(function() {
			var ItemActive = $('.steps__item-icon.active');

			if ( ItemActive.next().length > 0 ) {
				stepNext();
			} else {
				$('.steps__item-icon').removeClass('active, is-hide, right, left');
				stepCheck($('.steps__item-icon').first());
			}

			stepTimeout();
		}, 6000);
	}

	$('.steps__item-icon').on('click', function(e) {
		e.preventDefault();

		if ( $(this).hasClass('right') ) {
			stepNext();
		} else if ( $(this).hasClass('left') ) {
			stepPrev();
		}

		if ( Modernizr.mq('(min-width: 768px)') ) {
			stepCheck($(this))
		}

		clearTimeout(StepTimeout);
		stepTimeout();
	});

	$('.steps__icons').swipe({
		allowPageScroll: "vertical",
		swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
			if ( direction == 'left' ) {
				stepNext();
			} else if ( direction == 'right' ) {
				stepPrev();
			}

			clearTimeout(StepTimeout);
			stepTimeout();
	    }
	});
};
steps();


/* Reasons block */
function reasons() {
	$(window).on('load', function() {
		$('.reasons__item').first().addClass('active');
	});

	$('.reasons__item').on('mouseover', function(e) {
		if ( Modernizr.mq('(min-width: 992px)') ) {
			$('.reasons__item').removeClass('active');
			$(this).addClass('active');
		}
	});
};
reasons();


/* Examples slideshow */
$(window).on('load', function() {
	// Slider of best examples
	var ecampleSlider = new Swiper ('.examples__slides', {
			autoHeight: true,
	        paginationClickable: true,
	        preloadImages: false,
	        lazyLoading: true,
			nextButton: '.swiper-button-next',
	    	prevButton: '.swiper-button-prev',
	    	onInit: function(swiper) {
	    		$(swiper.slides).each(function() {
	    			if ( $(this).find('.examples__slides__item__gallery')[0] ) {
		    			$(this).find('.examples__slides__item__image').append('<div class="examples__slides__item__image__count">'+ $(this).find('.modal-img').length +' фото</div>');
		    		}
	    		})
	    	}
		});

	// Slider reviews and comments
	var reviewSlider = new Swiper ('.reviews__slides', {
			autoHeight: true,
	        spaceBetween: 50,
	        paginationClickable: true,
			nextButton: '.swiper-button-next',
	    	prevButton: '.swiper-button-prev',
	    	pagination: '.swiper-pagination'
		});


// libs.js:7 TypeError: Attempted to assign to readonly property.prop @ libs.js:7T @ libs.js:6prop @ libs.js:7afterClose @ common.js:225trigger @ libs.js:75cleanUp @ libs.js:75h @ libs.js:75(anonymous function) @ libs.js:75dispatch @ libs.js:6handle @ libs.js:6trigger @ libs.js:7(anonymous function) @ libs.js:7each @ libs.js:5each @ libs.js:5trigger @ libs.js:7(anonymous function) @ libs.js:75


	// Modal gallery Fancybox
	var imageFancybox = $(".modal-img").fancybox({
			loop: true,
			arrows: true,
			keyboard: true,
			animationEffect : "zoom",
			autoFocus : false,
			touch : {
				vertical : false,
			},
			image : {
				protect : true
			},
			afterShow: function(instance, slide) {},
			afterClose: function() {}
		});

	$.fancybox.defaults.afterClose = function() {
		$(this.src).removeAttr('style')
	}
});


/* Cover or contain background of image */
function bgCover() {
	if ( !$('.cover')[0] ) {
		return;
	}

	$('.cover').each(function() {
		$(this).css({
			backgroundImage: function() {
				return 'url('+$(this).find('>img').attr('src')+')';
			},
			backgroundRepeat: 'no-repeat',
			backgroundPosition: 'center',
			backgroundSize: 'cover'
		}).addClass('js-loaded').find('>img').hide();
	})
}
function bgContain() {
	if ( !$('.contain')[0] ) {
		return;
	}

	$('.contain').each(function() {
		$(this).css({
			backgroundImage: function() {
				return 'url('+$(this).find('>img').attr('src')+')';
			},
			backgroundRepeat: 'no-repeat',
			backgroundPosition: 'center',
			backgroundSize: 'contain'
		}).addClass('js-loaded').find('>img').hide();
	})
}
$(window).on('load', function() {
	bgCover();
	bgContain();
});


/* Contact form */
(function($) {
	var MaskPhone = "(999) 999-99-99";
	var FileSizeLimit = 3; // Мегабайти

	// Метод валідації поля телефона з маскою
	$.validator.addMethod('validatorPhoneMask', function(value, element) {
		return this.optional(element) || value.replace(/[^0-9.]/g, "").length == MaskPhone.replace(/[^0-9.]/g, "").length;
	}, 'Введите номер телефона');

	// Метод валідації максимального розмыру файлу
	$.validator.addMethod('filesize', function (value, element, param) {
	    return this.optional(element) || Math.pow(element.files[0].size/(1024*1000), 1).toPrecision(3) <= param;
	}, 'Файл должен быть меньше чем {0} mb');

	// Розшірення масиву типовіх повідомлень про валідацію
	$.extend($.validator.messages, {
		required: 'Заполните это поле'
	});

	// var FormValidator = $('.contactform form').validate({
	var ValidateOptions = {
		errorClass: 'has-error',
		focusCleanup: true,
		rules: {
			user_name: {
				required: true,
				minlength: 4,
			},
			user_phone: {
				required: true,
				validatorPhoneMask: true,
			},
			user_file: {
				accept: "image/jpeg, image/jpg, image/png",
				filesize: FileSizeLimit
			},
			// "hiddenRecaptcha": {
			// 	required: function() {
			// 		if(grecaptcha.getResponse() == '') {
			// 			return true;
			// 		} else {
			// 			return false;
			// 		}
			// 	}
			// }
		},
		messages: {
			user_name: {
				required: 'Введите Ваше имя и фамилию',
				minlength: 'Имя слишком короткое',
			},
			user_phone: {
				required: 'Введите номер телефона',
			},
			user_file: {
				accept: 'Доступные форматы JPG, JPEG, PNG',
			}
		},
		highlight: function(element, errorClass, validClass) {
			if ( errorClass ) {
				$(element).closest(".form-group").addClass(errorClass);
			}
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).closest(".form-group").removeClass(errorClass).find('.has-error').remove();
		},
		onfocusin: function(element, event) {
			this.element(element);
			$(element).closest('.form-group').addClass('is-focused');
		},
		onfocusout: function(element, event) {
			this.element(element);
			$(element).closest('.form-group').removeClass('is-focused');
		},
		// onclick: function(element, event) {},
		onkeyup: function(element, event) {
			this.element(element);
			checkEmptyField(element, event);
		},
		errorPlacement: function(error, element) {
			if( $(element).closest('.form-group').length ) {
				
		        error.insertAfter(element);
	        }
		},
		submitHandler: function(form) {
			return false;
		}
	};

	$('.contactform form').each(function(index, el) {
		$(this).validate(ValidateOptions);
	});

	// Костиль для перевірки чи пусте значення поля
	function checkEmptyField(element, event) {
		var Element = $(element),
			ElementValue = Element.val(),
			FormGroup = Element.closest('.form-group');

		// Спеціально для поля телефону
		if ( $(element).is('[type=tel]') && event ) {
			ElementValue = ElementValue.replace(/[^0-9.]/g, "");
			if ( event && event.type == 'keyup' && !Element.valid() ) {
				ElementValue = '';
			}
		}
		
		// Спеціально для поля файлу
		if ( $(element).is('[type=file]') ) {
			if ( element[0].files[0] ) {
				ElementValue = true;
			} else {
				ElementValue = '';
			}
		}

		if ( ElementValue === null || ElementValue == "undefined" || ElementValue === "") {
			FormGroup.addClass('is-empty');
		} else {
			FormGroup.removeClass('is-empty');
		}
	}

	// Перевірка пустих полів всієї форми
	function checkFormEmptyFields(Class) {
		$(Class).each(function() {
			$(this).find('input, textarea').each(function() {
				checkEmptyField($(this))
			});
		});
	}

	// Додати файл до форми і показати превюху
	function fileAdd(e) {
		if ( e.target.files[0] ) {
			var File = e.target.files[0],
	        	Reader = new FileReader(),
	        	Result = false,
	        	Message = '',
	        	Form = $(e.target).closest('form');

			if ( File && $(e.target).valid() ) {
		        var Container = $(e.target).closest('.contactform__file'),
		        	ContainerDel = Container.find('.contactform__file__del');

		        Container.addClass('is-loading');
		        Reader.onload = function(e) {
	                ContainerDel.css({
	                	backgroundImage: 'url('+e.target.result+')'
	                });
	                ContainerDel.find('span').text(File.type.substring(File.type.lastIndexOf('/') + 1)+' - '+ Math.pow(File.size/(1024*1000), 1).toPrecision(3) +' mb');
	                Container.removeClass('is-loading').addClass('is-attached');
	            };

	            Reader.readAsDataURL(File);
	        } else {
	        	$(e.target).val('');
	        }
        } else {
        	fileDel(e);
        }
	}

	// Видалити файл и превюху
	function fileDel(e) {
		var Input = $(e.target).closest('.contactform__file').find('input[type="file"]');

		if ( !Input.is(':disabled') ) {
			$(e.target)
				.closest('.contactform__file')
				.find('.contactform__file__del')
				.removeAttr('style')
				.closest('.contactform__file')
				.removeClass('is-loading is-attached')

			Input.val('');

			checkEmptyField(Input);
		}
	}


	function submitFormBeforeSend(Form) {
		Form.find('input, textarea, button').attr('disabled', true);
		Form.find('.contactform__file').addClass('is-disabled');
		Form.find('.contactform__btn-submit').addClass('is-loading');

		// grecaptcha.reset();
		// $('.contactform__btn-submit').parent().removeClass('is-animated').closest('form').find('.contactform__cnt-captcha').removeClass('is-animated');
	}


	function submitFormAfterSend(Form) {
		Form.find('input, textarea, button').attr('disabled', false);
		Form.find('.contactform__file').removeClass('is-disabled');
		Form.find('.contactform__btn-submit').removeClass('is-loading');
		Form.find('input[type="file"]').trigger('change');
	}


	function submitFormSuccess(Form) {
		$.fancybox.close();
		Form[0].reset();
		checkFormEmptyFields('.contactform form');
		swal({
            title: 'Ваше сообщение отправлено!',
            html: '<p>Я отвечу Вам, как только вымою руки от краски.</p>',
            type: 'success',
            confirmButtonText: 'Закрыть',
        })
        .catch(swal.noop);
	}


	function submitFormError() {
		$.fancybox.close();
		swal({
            title: 'Ошибка!',
            html: '<p>Что-то пошло не так. Попробуйте позже.</p>',
            type: 'error',
            confirmButtonText: 'Закрыть',
        })
        .catch(swal.noop)
	}


	function submitFormWarning(Text) {
		$.fancybox.close();
		swal({
            title: 'Внимание!',
            html: '<p>'+Text+'</p>',
            type: 'warning',
            confirmButtonText: 'Закрыть',
        })
        .catch(swal.noop)
	}


	function submitFormModalShow(e) {
		e.preventDefault();

		$('body').addClass('is-overflow');
		$('.modal_contactform').stop(true).fadeIn(1, function() {
			$(this).addClass('is-show');
		})
	}


	function submitFormModalHide(e) {
		e.preventDefault();

		$('body').removeClass('is-overflow');
		$('.modal_contactform').removeClass('is-show').delay(300).queue(function(next) {
			$(this).hide();
			next();
		});
	}


	function submitForm(e) {
		var Form = $(e.target).closest('form');

		if ( Form.valid() === true ) {
			var Data = new FormData();
			
			Data.append('action', 'mail');

			var DataOther = Form.serializeArray();
			$.each(DataOther, function(key,input) {
				Data.append(input.name, input.value);
			});

			if ( Form.find('input[type="file"]')[0].files[0] ) {
		        Data.append('user_file', Form.find('input[type="file"]')[0].files[0]);
		    }

			$.ajax({
	            url: 'mail/mail.php',
	            type: 'POST',
	            dataType: 'JSON',
	            data: Data,
	            enctype: 'multipart/form-data',
	            processData: false,
	            contentType: false,
	            cache: false,
	            beforeSend: function() {
	            	submitFormBeforeSend(Form);
	            },
	            success: function(Result) {
	            	if ( Result.ok ) {
	            		submitFormSuccess(Form);
	            	} else {
	            		if ( Result.validate ) {
	            			submitFormWarning(Result.text);
	            		} else {
	            			submitFormError();
	            		}
	            	}
	            },
	            complete: function(jqXHR, textStatus) {
            		submitFormAfterSend(Form);
	            },
	            error: function(jqXHR, textStatus, errorThrown) {
	            	console.log('Ajax error textStatus:', textStatus)
	            	submitFormError();
	            }
	        });

	        return false;
		}
	}


	$.contactForm = {
		init: function() {
			// Призначаємо маску для поля телефону
			$('input[type="tel"]').mask(MaskPhone, {allowBad: true});
			
			// Перевіряємо чи пусті поля форми
			checkFormEmptyFields('.contactform form');

			// Events methods
			$(document)
			.on('click', '.contactform__file__add', function() {
				$(this).closest('.contactform__file').find('input').trigger('click');
			})
			.on('change', '.contactform__file input', fileAdd)
			.on('click', '.contactform__file__del', fileDel)
			.on('click', '.contactform__btn-submit', submitForm)
			.on('keyup, keydown, blur, change', '.contactform .form-control', function(e) {
				checkEmptyField($(e.target));
			})
			.on('focusout', '.contactform .form-control', function(e) {
				$(e.target).closest('.form-group').removeClass('is-focused');
			})
			.on('click', '.calltoform__btn', submitFormModalShow)
			.on('click', '.modal_contactform_close', submitFormModalHide)
		}
	}
})(jQuery);

// Contact form init
$($.contactForm.init);


/* Parallax */
var rellax = new Rellax('.rellax');





