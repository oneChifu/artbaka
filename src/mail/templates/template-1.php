<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="format-detection" content="telephone=no">
	<title>Письмо от Artbaka.com.ua</title>

	<link href="../../css/mail.css" rel="stylesheet" />
</head>

<body style="margin:0; padding:0;" bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
	<tr>
		<td class="main" align="center" valign="top" bgcolor="#ffffff" style="background-color: #ffffff;">
			<div class="head">
				<div class="title">Вам письмо с сайта<br><a href="<?php echo $data['main_url']; ?>" target="_blank"><?php echo $data['main_url_title']; ?></a></div>
				<div class="subtitle">Возможно это заказ, не игнорируйте ;)</div>
			</div>

			<table class="content">
				<tr>
					<td class="content__title">От:</td>
					<td class="content__value"><?php echo $data['user_name']; ?></td>
				</tr>
				<tr>
					<td class="content__title">Его номер телефона:</td>
					<td class="content__value"><a href="callto:<?php echo $data['user_phone']; ?>"><?php echo $data['user_phone']; ?></a></td>
				</tr>
				<tr>
					<td class="content__title">Его IP:</td>
					<td class="content__value"><?php echo $data['user_ip']; ?></td>
				</tr>
				<?php if ( $data['user_message'] != '' ) : ?>
					<tr>
						<td class="content__title">Также он пишет:</td>
						<td class="content__value"><?php echo $data['user_message']; ?></td>
					</tr>
				<?php endif; ?>
				<?php if ( $data['user_file'] != '' ) : ?>
					<tr>
						<td class="content__center" colspan="2">Ещё он прислал картинку</td>
					</tr>
				<?php endif; ?>
			</table>

			<div class="social">
                <a href="http://vk.com/clubfairyart" target="_blank" class="social__item" title="Vkontakte"><img src="<?php echo $data['main_url']; ?>/img/icons/icon-social-vk-hover.png" alt="Vkontakte"></a>
                <a href="https://www.instagram.com/graffiti_artbaka/" target="_blank" class="social__item" title="Instagram"><img src="<?php echo $data['main_url']; ?>/img/icons/icon-social-ig-hover.png" alt="Instagram"></a>
                <a href="https://www.facebook.com/people/%D0%90%D0%BB%D0%B5%D0%BA%D1%81%D0%B5%D0%B9-%D0%91%D0%B0%D0%BA%D0%B0/100005350004824" target="_blank" class="social__item" title="Facebook"><img src="<?php echo $data['main_url']; ?>/img/icons/icon-social-fb-hover.png" alt="Facebook"></a>
            </div>

            <div class="footer">
            	Наши телефоны: <br><a href="callto:+380962796005">096 279-60-05</a><br><a href="callto:+380996762122">099 676-21-22</a>
            </div>
		</td>
	</tr>
</table>
</body>
</html>